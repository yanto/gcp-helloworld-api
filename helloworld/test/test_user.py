from datetime import datetime
import os
import sys
import unittest

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from app import app, db, user
from config import TestConfig


class UserTest(unittest.TestCase):

    def setUp(self):
        app.config.from_object(TestConfig)
        self.app = app.test_client()

        db.drop_all()
        db.create_all()

    def test_birthday_today(self):
        username = 'testuser'
        today = datetime.today().date()
        birthday_today = datetime(today.year-50, today.month, today.day).date()

        response = self.app.put('/hello/{}'.format(username), json={
            'dateOfBirth': birthday_today.strftime('%Y-%m-%d')
        })
        self.assertEqual(204, response.status_code)
        self.assertIsNone(response.json)

        response = self.app.get('/hello/{}'.format(username))
        self.assertEqual(200, response.status_code)
        self.assertTrue('Happy birthday!' in response.json.get('message'))

    def test_birthday_not_today(self):
        username = 'testuser'
        today = datetime.today().date()
        birthday_next_week = datetime(today.year-80, today.month, today.day+7).date()

        response = self.app.put('/hello/{}'.format(username), json={
            'dateOfBirth': birthday_next_week.strftime('%Y-%m-%d')
        })
        self.assertEqual(204, response.status_code)
        self.assertIsNone(response.json)

        response = self.app.get('/hello/{}'.format(username))
        self.assertEqual(200, response.status_code)
        self.assertFalse('Happy birthday!' in response.json.get('message'))
        self.assertTrue('Your birthday is in 7 day(s).' in response.json.get('message'))

    def test_wrong_date_format(self):
        username = 'testuser'

        response = self.app.put('/hello/{}'.format(username), json={
            'dateOfBirth': 'lastyear'
        })
        self.assertEqual(200, response.status_code)
        self.assertTrue('"dateOfBirth" is not given or wrong.' in response.json.get('message'))

    def test_get_non_existing_user(self):
        username = 'young'
        response = self.app.get('/hello/{}'.format(username))
        self.assertEqual(200, response.status_code)
        self.assertEqual('User not found.', response.json.get('message'))


if __name__ == '__main__':
    unittest.main()
