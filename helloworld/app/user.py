from app import db


class User(db.Model):
    username = db.Column(db.String(200), unique=True, nullable=False, primary_key=True)
    dob = db.Column(db.Date, nullable=False)


    def __repr__(self):
        return "<Username: {}, DOB: {}>".format(self.username, self.dob.strftime('%Y-%m-%d'))
