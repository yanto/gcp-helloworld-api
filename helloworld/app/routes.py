from datetime import datetime
from flask import jsonify, request
import http

from app import app, db, user


@app.route('/')
def index():
    return jsonify({'message': 'Hello World!'})


@app.route('/hello/<username>', methods=['PUT', 'GET'])
def hello(username):
    if request.method == 'PUT':
        try:
            dob = datetime.strptime(request.get_json().get('dateOfBirth'), '%Y-%m-%d').date()
            the_user = user.User.query.filter_by(username=username).first()
            if not the_user:
                the_user = user.User(username=username, dob=dob)
            else:
                the_user.dob = dob
            db.session.add(the_user)
            db.session.commit()
            return '', http.HTTPStatus.NO_CONTENT
        except:
            return jsonify({'message': '"dateOfBirth" is not given or wrong.'})
    elif request.method == 'GET':
        the_user = user.User.query.filter_by(username=username).first()
        if the_user:
            dob = the_user.dob
            today = datetime.today().date()
            birthday_this_year = datetime(today.year, dob.month, dob.day).date()
            birthday_next_year = datetime(today.year+1, dob.month, dob.day).date()
            daysdiff = (birthday_this_year - today).days if ((birthday_this_year - today).days >= 0) else (birthday_next_year - today).days
            if daysdiff == 0:
                msg = 'Hello, {}! Happy birthday!'.format(the_user.username)
            else:
                msg = 'Hello, {}! Your birthday is in {} day(s).'.format(the_user.username, daysdiff)
            return jsonify({'message': msg})
        else:
            return jsonify({'message': 'User not found.'})
