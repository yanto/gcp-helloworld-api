import os


class Config(object):
    PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'this-is-my-secret-key'
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(PROJECT_DIR, 'db.sqlite3'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(object):
    PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'this-is-my-secret-key'
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(PROJECT_DIR, 'test.sqlite3'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = True
    DEBUG = False
