# HelloWorld API

This repository contains the implementation of HelloWorld API. The API is written in Python using the Flask micro-framework. A Dockerfile has been written that will package the API into a Docker image.

The `deployment` directory contains the Kubernetes manifests that allow the API to be deployed on a Kubernetes cluster. The deployment structure is described by the following diagram:

```
 ------------------- helloworld-api-cluster ----------------
|                                                           |
|   ------ uWSGI -------                                    |
|  |                    |                                   |
|  |  ------- Flask --  |                  -- nginx --                     ---------------
|  | | helloworld-api | |  5000 <===> 80  |           |  cluster IP <===> | nginx-ingress | public IP <===>  client
|  |  ----------------  |                  -----------                     ---------------
|  |                    |                                   |
|   --------------------                                    |
|                                                           |
 -----------------------------------------------------------

```

## Deployment Steps

### Pre-requisites

* Kubernetes cluster. This setup has been tested on [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine).
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [Docker](https://www.docker.com/)
* [Ansible](https://www.ansible.com/)
* Python 3.x

NOTE: The Ansible playbook will deploy to the Kubernetes cluster that is set as the default in kubeconfig.

To deploy, simply execute the playbook:

```bash
ansible-playbook -i inventory.yaml deploy-gcp-playbook.yaml
```
